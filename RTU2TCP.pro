#-------------------------------------------------
#
# Project created by QtCreator 2018-01-15T21:55:45
#
#-------------------------------------------------

QT       += core gui
CONFIG   += serialport
QT       += sql

greaterThan(QT_MAJOR_VERSION, 4): {QT += widgets
}
else {
!mac:qtAddLibrary(QtSerialPort)

!isEmpty(QTSERIALPORT_BUILD_ROOT) {
    INCLUDEPATH -= $$QMAKE_INCDIR_QT/QtSerialPort
    QMAKE_INCDIR += $$QTSERIALPORT_BUILD_ROOT/include $$QTSERIALPORT_BUILD_ROOT/include/QtSerialPort

    QTSERIALPORT_BUILD_SUBDIR = src/serialport
    debug_and_release_target {
        CONFIG(debug, debug|release) {
            QTSERIALPORT_BUILD_SUBDIR = $$QTSERIALPORT_BUILD_SUBDIR/debug
        } else {
            QTSERIALPORT_BUILD_SUBDIR = $$QTSERIALPORT_BUILD_SUBDIR/release
        }
    }

    QMAKE_LIBDIR += $$QTSERIALPORT_BUILD_ROOT/$$QTSERIALPORT_BUILD_SUBDIR
}

mac {
    INCLUDEPATH += $$[QT_INSTALL_HEADERS]/QtSerialPort

   if(!debug_and_release|build_pass):CONFIG(debug, debug|release) {
       LIBS += -lQtSerialPort$${QT_LIBINFIX}_debug
   } else {
       LIBS += -lQtSerialPort$${QT_LIBINFIX}
   }
}

}


TARGET = RTU2TCP
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    libmodbus/modbus-data.c \
    libmodbus/modbus-rtu.c \
    libmodbus/modbus-tcp.c \
    libmodbus/modbus.c \
    comtcp.cpp

HEADERS  += mainwindow.h \
    libmodbus/config.h \
    libmodbus/modbus-private.h \
    libmodbus/modbus-rtu-private.h \
    libmodbus/modbus-rtu.h \
    libmodbus/modbus-tcp-private.h \
    libmodbus/modbus-tcp.h \
    libmodbus/modbus-version.h \
    libmodbus/modbus.h \
    comtcp.h

FORMS    += mainwindow.ui

LIBS += -Ldll -lws2_32
