#ifndef COMTCP_H
#define COMTCP_H

#include <QObject>
#include <QThread>
#include "libmodbus/modbus.h"

class ComTcp : public QThread
{
public:
    enum    ComMode
    {
        mode_in,mode_out
    };
    ComTcp(QString IP,int Port,uint16_t *database,QString serialname,int id,ComMode wormode);
private:
    QString IPAddress;
    QString serialname;
    int     slaveID;
    int     Port;
    uint16_t *database;
    modbus_t *mb;
    modbus_t *mbrtu;
    modbus_mapping_t *mb_mapping;
    ComMode workmode;
    // QThread interface
protected:
    void run();
};

#endif // COMTCP_H
